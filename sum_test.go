package calc

import (
	"testing"
)

func TestSum(t *testing.T) {
	actual := sum(1, 2)
	expected := 3
	if actual != expected {
		t.Errorf("sum(1, 2): actual %v, expected %v", actual, expected)
	}
}

func TestSubstratction(t *testing.T) {
	actual := subtraction(3, 2)
	expected := 1
	if actual != expected {
		t.Errorf("subtraction(3, 2): actual %v, expected %v", actual, expected)
	}
}

func TestMultiplication(t *testing.T) {
	actual := multiplication(5, 2)
	expected := 10
	if actual != expected {
		t.Errorf("multiplication(5, 2): actual %v, expected %v", actual, expected)
	}
}

func TestDivision(t *testing.T) {
	actual := division(6, 2)
	expected := 3
	if actual != expected {
		t.Errorf("division(6, 2): actual %v, expected %v", actual, expected)
	}
}
